import React from "react";
import "./NotFound.less";

export default () =>
  <div className="NotFound">
    <h3>Sorry, page not found!</h3>
  </div>;